module HanoiTowers
	class Tower
		attr_accessor :blocks

		def initialize(block_count)
			@blocks = []

			block_count.downto 1 do |i|
				@blocks << i
			end
		end

		def push_block(block)
			@blocks << block
		end

		def pop_block
			@blocks.pop
		end

		def top
			@blocks.last
		end

		def print
			retval = ""
			@blocks.each do |b|
				retval += "#{b} "
			end

			retval
		end
	end

	class Manager
		attr_accessor :game

		def can_move?(from, to)
			from_block = @game.towers[from].top
			to_block = @game.towers[to].top

			if @game.game_state != :finished &&
				(to_block.nil? || to_block > from_block)
				return true
			end

			false
		end

		def change_state?
			if @game.towers[0].top.nil? &&
				(@game.towers[1].top.nil? || @game.towers[2].top.nil?)
				return true
			end

			false
		end
	end

	class Game
		attr_accessor :difficulty, :towers, :manager, :game_state, :steps_made

		def initialize(difficulty)
			@manager = Manager.new
			r(difficulty)
			print
		end

		def m(from, to)
			retval = process_move(from-1, to-1)
			print
			retval
		end

		def print
			3.times do |i|
				puts "[#{i+1}]: #{@towers[i].print}"
			end

			puts "difficulty: #{@difficulty}"
			puts "state: #{@game_state}"
			puts "steps: #{@steps_made}"
			true
		end

		def r(difficulty)
			@difficulty = difficulty
			@towers = []
			@game_state = :unstarted
			@steps_made = 0

			3.times do |i|
				@towers << Tower.new(i == 0 ? difficulty : 0)
			end

			@manager.game = self
		end

		private
			def process_move(from, to)
				if @manager.can_move?(from, to)
					@towers[to].push_block(@towers[from].pop_block)
					@game_state = :started if @steps_made == 0
					@steps_made += 1
					if @manager.change_state?
						@game_state = :finished
						return :finished
					end

					return :moved
				end

				:not_allowed
			end
	end
end