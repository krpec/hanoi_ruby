# README #

Simple Hanoi towers game written in ruby, running in irb.

### Usage ###


```
#!ruby

load './game.rb'
game_difficulty = 3
g = HanoiTowers::Game.new(game_difficulty)
g.m(1, 2) #move disc from 1st to 2nd tower
g.print #print current game information
g.r(game_difficulty) #reset game
```